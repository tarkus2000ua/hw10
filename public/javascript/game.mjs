import {
  renderRoomList
} from './components/room-list/room-list.js';
import {
  renderRoom,
  createUserList
} from './components/room/room.js';
import {
  updateUserProgress,
  updateUserTime
} from './socket/socketHandler.js';

const username = sessionStorage.getItem("username");
export const Player = {
  name: username,
  isready: false,
  progress: 0,
  room: 'WAITING_ROOM',
  time: null,
  charLeft: null
}

if (!username) {
  window.location.replace("/login");
}

export const socket = io("", {
  query: {
    username
  }
});

socket.on('commentator', message => {
  const commentsBox = document.getElementById('comments-box');
  commentsBox.innerHTML = message;
  responsiveVoice.speak(message, "Ukrainian Female", {rate: 1.2});
})

socket.on('userExisted', message => {
  alert(message);
  sessionStorage.removeItem("username");
  window.location.replace("/login");
});

socket.on('roomExisted', message => {
  alert(message);
});

socket.on('roomsInfo', roomsInfo => {
  renderRoomList(roomsInfo);
});

socket.on('room', roomInfo => {
  Player.isready = roomInfo.users.find(user => user.name === Player.name).isready;
  Player.progress = roomInfo.users.find(user => user.name === Player.name).progress;
  Player.charLeft = roomInfo.users.find(user => user.name === Player.name).charLeft;
  Player.room = roomInfo.name;
  renderRoom(roomInfo);
});

socket.on('progress', roomInfo => {
  const userList = document.querySelector('.user-list');
  const updatedUserList = createUserList(roomInfo.users);
  userList.parentNode.replaceChild(updatedUserList, userList);
});

socket.on('gamePrepare', async (textId) => {
  let response = await fetch(`/game/texts/${textId}`);
  let text = await response.text();
  const textBox = document.getElementById('text-box');
  textBox.innerHtml = '';

  text.split('').forEach(char => {
    const charSpan = document.createElement('span');
    charSpan.innerText = char;
    textBox.appendChild(charSpan);
  });

  const timer = document.getElementById('timer');
  const buttons = document.getElementsByClassName('btn');
  for (let button of buttons) {
    button.classList.toggle('hide');
  }
  timer.classList.toggle('hide');
  const timeLeft = document.getElementById('time-left');
  timeLeft.classList.toggle('hide');
})

socket.on('gameEnded', (textId) => {
  new Audio('../assets/audio/finish.mp3').play();
  const backBtn = document.querySelector('.btn.back');
  backBtn.classList.toggle('hide');

  Player.isready = 'false';
  Player.progress = null;
  Player.charLeft = null;
  Player.time = null;
});

socket.on('timer', value => {
  new Audio('../assets/audio/click.mp3').play();
  timer.innerText = value
});

socket.on('timeLeft', value => {
  const timeLeft = document.getElementById('time-left');
  timeLeft.innerText = 'Time left:'+value;
  Player.time = value;
});

socket.on('gameStart', value => {
  const timer = document.getElementById('timer');
  const textBox = document.getElementById('text-box');
  timer.classList.toggle('hide');
  textBox.classList.toggle('hide');

  let text = textBox.innerText.split('');
  let progress = 0;
  const rate = 100 / text.length;
  let charsLeft = text.length;
  let index = 0;

  let spans = textBox.querySelectorAll('span');
  spans[index].classList.add('next');
  window.addEventListener('keydown', event => keyDownHandler(event));

  const keyDownHandler = (e) => {
    function  keySound(){
      new Audio('../assets/audio/key.mp3').play();
    }
    const throttledKeySound = _.throttle(keySound, 1500);
    throttledKeySound();
    if (e.key === text[index]) {
      spans[index].classList.remove('next');
      spans[index].classList.add('highlight');
      if (index === text.length-1) {
        progress = 100;
        charsLeft = 0;
        updateUserProgress(Player.room, progress, charsLeft);
        updateUserTime(Player.room, Player.time);
        window.removeEventListener('keydown', event => keyDownHandler(event));

      } else {
        index++;
        spans[index].classList.add('next');
        progress += rate;
        charsLeft -=1;
        updateUserProgress(Player.room, progress, charsLeft );
      }
    }
  }
});