import { createElement } from '../../helpers/domHelper.js';

export const createCommentator = () => {
  const commentator = createElement({ tagName: 'div', className: 'commentator' });
  const picture = createElement({ tagName: 'img', className: 'picture', attributes: { src: '../assets/img/commentator.png'} });
  const commentsBox = createElement({ tagName: 'div', className: 'comments-box', attributes:{id: 'comments-box'} });
  commentator.append(picture,commentsBox);
  return commentator;
}