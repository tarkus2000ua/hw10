import {
  roomsInfo,
  unlockRoom,
  resetUsers,
  getGameResults,
  lockRoom,
  checkIsEverybodyCompleted
} from './roomService.js';
import {
  getRandomTextId
} from './textService.js';
import {
  SECONDS_TIMER_BEFORE_START_GAME,
  SECONDS_FOR_GAME
} from '../socket/config.js';
import { Commentator } from './commentator.js';

export class Game {
  constructor(room, io) {
    this.timer = null;
    this.commentator = null;
    this.room = room;
    this.io = io;
  }

  async start() {
    // Set room status "locked". Hide it from users in the waiting room
    lockRoom(this.room);
    this.io.to('WAITING_ROOM').emit('roomsInfo', roomsInfo());

    // Send start timer messages to room
    this.io.to(this.room).emit('gamePrepare', getRandomTextId());
    await this.countdown(SECONDS_TIMER_BEFORE_START_GAME, 'timer');

    this.commentator = new Commentator(this.room, this.io);
    this.commentator.start();
    const results = await Promise.race([this.gameTillTimerEnd(), this.gameTillEverybodyCompleted()]);

    this.commentator.clearTimer();
    this.clearTimer();
    resetUsers(this.room);
    unlockRoom(this.room);
    this.io.to(this.room).emit('gameEnded', results);
    return results;
  }

  async gameTillTimerEnd() {
    return new Promise(async (resolve) => {
      // Start game
      this.io.to(this.room).emit('gameStart', 'start');
      await this.countdown(SECONDS_FOR_GAME, 'timeLeft')
      resolve(getGameResults(this.room));
    });
  }

  async gameTillEverybodyCompleted() {
    return new Promise((resolve) => {
        let i=0;
        setInterval(() => {
          if (checkIsEverybodyCompleted(this.room)) {
            clearInterval(this.timer);
            resolve(getGameResults(this.room));
          } else {
            i++;
          }
        },1000)
      })
  }

  async countdown(startTime, message) {
    return await new Promise(resolve => {
      let i = startTime;
      this.timer = setInterval(() => {
        this.io.to(this.room).emit(message, i);
        i--;

        if (i < 0) {
          clearInterval(this.timer);
          resolve('ok');
        }
      }, 1000);
    });
  }

  clearTimer() {
    clearInterval(this.timer);
  }

}