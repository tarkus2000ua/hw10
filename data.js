export const texts = [
  "Your limitation - it's only your imagination",
  "Push yourself, because no one else is going to do it for you",
  "Sometimes later becomes never. Do it now",
  "Great things never come from comfort zones",
  "Dream it. Wish it. Do it",
  "Success doesn't just find you. You have to go out and get it",
  "Little things make big days",
];

export const rooms = [
  {
    "name": "WAITING_ROOM",
    "users":[]
  },
  {
  "name": "room1",
  "status":"free",
  "users": [{
    "name":"John",
    "isready": true,
    "progress":50,
    "time":null,
    "charsLeft": null
  },
  {
    "name":"Brad",
    "isready": true,
    "progress":70,
    "time":null,
    "charsLeft": null
  },
  {
    "name":"Ivan",
    "isready": true,
    "progress":0,
    "time":null,
    "charsLeft": null
  }]
},
{
  "name": "room2",
  "status":"busy",
  "users": [{
    "name":"Jenny",
    "isready": false,
    "progress":0,
    "time":null,
    "charsLeft": null
  },
  {
    "name":"Olga",
    "isready": false,
    "progress":0,
    "time":null,
    "charsLeft": null
  },
  {
    "name":"Eve",
    "isready": false,
    "progress":0,
    "time":null,
    "charsLeft": null
  }]
},
{
  "name": "room3",
  "status": "free",
  "users": [{
    "name":"Peter",
    "isready": true,
    "progress":100,
    "time":null,
    "charsLeft": null
  }]
}]

export default { texts };
