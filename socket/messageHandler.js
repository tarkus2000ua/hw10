// Facade Pattern

import {
  roomsInfo,
  joinRoom,
  exitRoom,
  isUserExisted,
  isRoomExisted,
  userDisconnected,
  isReadyToStart,
  updateUserInfo,
  createRoom,
  removeEmptyRoom,
} from '../services/roomService.js';
import { Game } from '../services/gameService.js';
import { SECONDS_FOR_GAME } from '../socket/config.js';


export class MessageHandler {
  constructor(io, socket) {
    this.io = io;
    this.socket = socket;
    this.username = socket.handshake.query.username;
    this.rooms = io.sockets.adapter.rooms;
    this.game = null;
  }

  connectHandle() {
    if (isUserExisted(this.username)) {
      this.socket.emit('userExisted', `Username ${this.username} already existed on server. Please choose another name.`);
    } else {
      this.socket.join('WAITING_ROOM');
      joinRoom(this.username, 'WAITING_ROOM');
      this.socket.emit('roomsInfo', roomsInfo());
    }
  }

  joinRoomHandle(room) {
    this.socket.leave('WAITING_ROOM');
    const roomInfo = joinRoom(this.username, room);
    this.socket.join(room);
    this.io.to(room).emit('room', roomInfo);
    this.socket.to('WAITING_ROOM').emit('roomsInfo', roomsInfo());
  }

  createRoomHandle(room) {
    if (isRoomExisted(room)) {
      this.socket.emit('roomExisted', `Room ${room} already existed on server. Please choose another name.`);
    } else {
      createRoom(room);
      this.socket.leave('WAITING_ROOM');
      const roomInfo = joinRoom(this.username, room);
      this.socket.join(room);
      this.io.to(room).emit('room', roomInfo);
      this.socket.to('WAITING_ROOM').emit('roomsInfo', roomsInfo());
    }
  }

  exitRoomHandle(room) {
    this.socket.leave(room);
    const roomInfo = exitRoom(this.username, room);
    this.socket.join('WAITING_ROOM');
    joinRoom(this.username, 'WAITING_ROOM');

    if (!removeEmptyRoom(room)) {
      this.socket.to(room).emit('room', roomInfo);
    }
    this.io.to('WAITING_ROOM').emit('roomsInfo', roomsInfo());
  }

  async userStatusChangeHandle(room, status) {
    const roomInfo = updateUserInfo(this.username, room, {
      name: 'isready',
      value: (status != 'true')
    });
    this.io.to(room).emit('room', roomInfo);

    if (isReadyToStart(roomInfo)) {
      this.game = this.rooms[room].game;
      if (!this.game) {
        this.game = new Game(room, this.io);
        try {
          await this.game.start();
        } catch (error) {
          console.log(error);
        }
        this.game = null;
      }
    }
  }

  userFinishHandle(room,time){
    updateUserInfo(this.username, room, {
      name: 'time',
      value: SECONDS_FOR_GAME - time
    });
  }

  userProgressHandle(room,progress,charsLeft){
    let roomInfo = updateUserInfo(this.username, room, {
      name: 'progress',
      value: progress
    });
    roomInfo = updateUserInfo(this.username, room, {
      name: 'charsLeft',
      value: charsLeft
    });
    this.io.to(room).emit('progress', roomInfo);
  }

  disconnectHandle(){
    const roomInfo = userDisconnected(this.username);
    if (!removeEmptyRoom(roomInfo.name)) {
      this.socket.to(roomInfo.name).emit('room', roomInfo);
    } else {
      this.game?.commentator?.clearTimer();
      this.game?.clearTimer();
      this.game = null;
    }
    this.io.to('WAITING_ROOM').emit('roomsInfo', roomsInfo());
  }
}