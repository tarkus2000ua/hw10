// Using Facade's methods

import { MessageHandler } from './messageHandler.js';

export default io => {
  io.on("connection", socket => {

    let messageHandler = new MessageHandler(io,socket);
    messageHandler.connectHandle();

    socket.on('joinRoom', room => {
      messageHandler.joinRoomHandle(room);
    });

    socket.on('createRoom', room => {
      messageHandler.createRoomHandle(room);
    });

    socket.on('exitRoom', room => {
      messageHandler.exitRoomHandle(room);
    });

    socket.on('userStatus', async ({ room, status }) => {
      messageHandler.userStatusChangeHandle(room, status);
    });

    socket.on('userTime', ({ room, time }) => {
      messageHandler.userFinishHandle(room,time);
    })

    socket.on('userProgress', ({ room, progress, charsLeft }) => {
      messageHandler.userProgressHandle(room,progress,charsLeft);
    });

    socket.on('disconnect', () => {
     messageHandler.disconnectHandle();
     messageHandler = null;
    });

  });
};